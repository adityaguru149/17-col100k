a = [1 2 1 2; 1 2 3 1; 1 1 1 -1];
a=[0 0 0 1; 0 0 1 0; 0 1 1 -1; 0 1 1 0; 1 1 0 -1];

%generic function for line segment intersection
%[inf inf] if line segments do not meet or parallel
%[NaN NaN] if lines colinear
%[x y] if line segments meet
function point = intersectLineSegment(l1, l2)
    tol = 1e-14;
    x1 =  l1(1);
    y1 =  l1(2);
    dx1 = l1(3);
    dy1 = l1(4);
    x2 =  l2(1);
    y2 =  l2(2);
    dx2 = l2(3);
    dy2 = l2(4);
    
    par = (abs(dx1*dy2 - dx2*dy1) < tol);
    if par && (abs((x2-x1) * dy1 - (y2-y1) * dx1) < tol)
        point = [inf inf];
    elseif par
        point = [inf inf]; %[NaN NaN];
    else
        x = ((y2-y1)*dx1*dx2 + x1*dy1*dx2 - x2*dy2*dx1)/(dx2*dy1-dx1*dy2);
        y = ((x2-x1)*dy1*dy2 + y1*dx1*dy2 - y2*dx2*dy1)/(dx1*dy2-dx2*dy1);
        x1_=x1+dx1; x2_=x2+dx2; y1_=y1+dy1; y2_=y2+dy2;
        %check if intersection coordinates lie in the given segments
        if x<=max(x1,x1_) && x>=min(x1,x1_) && x<=max(x2,x2_) && x>=min(x2,x2_) && y<=max(y1,y1_) && y>=min(y1,y1_) && y<=max(y2,y2_) && y>=min(y2,y2_)
            point = [x y];
        else
            point = [inf inf];
        end;
    end;
end;

function flag = sameside(p1,p2,a,b)
    % sameside : returns true if the p1,p1 lie on same sides of the
    % edge ab and false otherwise
    p1(3) = 0; p2(3) = 0; a(3) = 0; b(3) = 0;
    cp1 = cross(b-a, p1-a);
    cp2 = cross(b-a, p2-a);
    if(dot(cp1, cp2) >= 0)
        flag = true;
    else
        flag = false;
    end
end
% triangle_intersection : returns true if the triangles overlap and false otherwise
% P1, P2: a 3 by 2 array (each), describing the vertices of a triangle,
% the first column corresponds to the x coordinates while the second column
% corresponds to the y coordinates 
function flag = triangle_intersection(P1,P2)
    if length(intersect(P1,P2,"rows")) > 0
        flag = true; return;
    end
    P1(4:5,:) = P1(1:2,:);
    P2(4:5,:) = P2(1:2,:);

    flag = true;
    % Testing all the edges of P1
    for i=1:3
        if(~sameside(P1(i,:), P2(1,:), P1(i+1,:), P1(i+2,:)) ...
                && sameside(P2(1,:), P2(2,:), P1(i+1,:), P1(i+2,:)) ...
                && sameside(P2(2,:), P2(3,:), P1(i+1,:), P1(i+2,:)))
            flag = false; return;
        end
    end
    % Testing all the edges of P2
    for i=1:3
        if(~sameside(P2(i,:), P1(1,:), P2(i+1,:), P2(i+2,:)) ...
                && sameside(P1(1,:), P1(2,:), P2(i+1,:), P2(i+2,:)) ...
                && sameside(P1(2,:), P1(3,:), P2(i+1,:), P2(i+2,:)))
            flag = false; return;
        end
    end
end

%x y dx dy format
%[0 2 2 -2; 0 0 3 0; 1 0 1 2]
%[0 2 2 -2; 0 0 3 0; 1 0 1 -2] % open
%[1 2 3 4; 5 3 2 1; 2 6 7 2; 5 8 3 6; 12 5 4 9]

a = input('');
n = size(a, 1);
if n<3, printf("0 0.0000 0\n"), return, end;
intn = {};
for i=1:n-1
    for j=i+1:n
        intn{i, j} = intersectLineSegment(a(i, :), a(j, :));
%        printf("%d %d\n", i, j);
    end;
end;

triangles={};
cnt=0; area= 0.0;
for i=1:n-2
    for j=i+1:n-1
        for k=j+1:n
            if intn{i, j} != [NaN NaN] && intn{i, j} != [inf, inf] && intn{i, k} != [NaN NaN] && intn{i, k} != [inf, inf] && intn{j, k} != [NaN NaN] && intn{j, k} != [inf, inf]
                if intn{i,j}==intn{j,k}, continue, end;
                cnt += 1;
                triangle=[intn{i, j}; intn{i, k}; intn{j, k}];
                triangles{cnt} = triangle;
                area+= abs(polyarea(triangle(:, 1), triangle(:, 2)));
            end;
        end;
    end;
end;

overlap = zeros(cnt, 1);
if cnt > 1
    for i=1:cnt-1
        for j=i+1:cnt
            if triangle_intersection(triangles{i}, triangles{j})
                overlap(i)=1; overlap(j)=1;
            end;
        end;
    end;
end;
non_overlap = cnt - sum(overlap);
printf("%d %.4f %d\n", cnt, area, non_overlap);