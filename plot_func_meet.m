p1 = [pi+1 1 pi+2 -14];
p2 = [sqrt(2) pi pi/2 1];
x1 = linspace(1,2);
y1 = polyval(p1, x1);
y2 = polyval(p2, x1);
plot(x1, y1, 'r')
hold
grid minor on
plot(x1, y2, 'b')
plot(x1, exp(x1), 'g')
plot(x1, sin(x1), 'm')
plot(x1, pow2(x1), 'y')
