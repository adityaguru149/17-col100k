a = input(''); b = input('');
function [s] = myAns(a, x)
    if length(a) == 0
        s = false;
        return
    elseif length(a)==1
        s = a(1)==x;
        return
    endif
    s = (a(1)==x || myAns(a(2:end), x));
    return
endfunction

function [s] = myPerm(a, b)
    if length(a)==0
        s = (length(b)==0)
        return
    elseif length(a) == 1
        s = myAns(b, a(1));
        return
    else
        s = (myAns(b, a(1)) && myPerm(a(2:end), b));
        return
    endif
endfunction
printf("%s", mat2str((myPerm(a, b) && myPerm(b, a)))) %logical