a=[0 0 0 1; 0 0 1 0; 0 1 1 -1; 0 1 1 0; 1 1 0 -1];
function point = intersectLineSegment(l1, l2)
    tol = 1e-14;
    x1 =  l1(1);
    y1 =  l1(2);
    dx1 = l1(3);
    dy1 = l1(4);
    x2 =  l2(1);
    y2 =  l2(2);
    dx2 = l2(3);
    dy2 = l2(4);
 
    par = (abs(dx1*dy2 - dx2*dy1) < tol);
    if par && (abs((x2-x1) * dy1 - (y2-y1) * dx1) < tol)
        point = [inf inf];
    elseif par
        point = [inf inf]; %[NaN NaN];
    else
        x = ((y2-y1)*dx1*dx2 + x1*dy1*dx2 - x2*dy2*dx1)/(dx2*dy1-dx1*dy2);
        y = ((x2-x1)*dy1*dy2 + y1*dx1*dy2 - y2*dx2*dy1)/(dx1*dy2-dx2*dy1);
        x1_=x1+dx1; x2_=x2+dx2; y1_=y1+dy1; y2_=y2+dy2;
        if x<=max(x1,x1_) && x>=min(x1,x1_) && x<=max(x2,x2_) && x>=min(x2,x2_) && y<=max(y1,y1_) && y>=min(y1,y1_) && y<=max(y2,y2_) && y>=min(y2,y2_)
            point = [x y];
        else
            point = [inf inf];
        end;
    end;
end;

intersectLineSegment(a(1, :), a(4, :))
intersectLineSegment(a(4, :), a(1, :))
intersectLineSegment(a(3, :), a(4, :))
intersectLineSegment(a(4, :), a(3, :))
%n = size(a, 1);
%for i=1:n-1
%    for j=2:n
%        if i<j, intersectLineSegment(a(i, :), a(j, :)), printf("%d %d\n", i, j); end;
%    end;
%end;

