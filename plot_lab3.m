x = 2*pi*linspace(0, 1, 100);
y1 = x+sin(x);
y2 = x.^2+cos(x)-3*x;
y3 = 2*x+sqrt(x);
plot(x, y1, "r-", "linewidth",5)
hold on;
plot(x, y2, "b:", "linewidth",5)
plot(x, y3, "g-.", "linewidth",5)
xlabel ("x(0 to 2*pi)");
ylabel ("Functions");
legend("x+sin(x)", "x2+cos(x)-3x", "2*x+sqrt(x)")
title("Lab3 plots")