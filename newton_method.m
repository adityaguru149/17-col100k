xyz=0;

f1 = @(x) (tan(x) - 2*x);
f2 = @(x) (65*x^4-72*x^3-1.5*x^2+16.5*x-1);
f3 = @(x) (x^3-6*x^2+12*x-8);
f1p = @(x) (sec(x)^2 - 2);
f2p = @(x) (260*x^3-216*x^2-3*x+16.5);
f3p = @(x) (3*x^2-12*x+12);

function [y,iter]=newton(f,fp,x0,tol)
    max=100;  % The maximum permited number of iterations
    iter=0;   % We start on the first interation
    if (feval(fp,x0)==0)
    %  Check to see before we get div by 0 erro
        y=-1; return
    end
    x1=x0-feval(f,x0)/feval(fp,x0);  % The first Newton step
    while(abs((x1-x0)/x0)>tol)
        iter += 1;
        x0=x1; % update the guess
        if (feval(fp,x0)==0)
        %  Check to see before we get div by 0 erro
            y=-1; return
        end
        x1=x0-feval(f,x0)/feval(fp,x0); % next iteration
        if (iter>=max)
        y=-1; return
        end
    end
    y=x1;  % return solution
    return
end;

f = input('');
fp = input('');
x0 = input('');
tol = input('');
[ans, i] = newton(f, fp, x0, tol);
printf("%.4f\n%d\n",ans, i)