n = input('')
function [f] = fact_rec(n)
    if(n<2)
        f=1; return;
    else
        f=n*fact_rec(n-1);
    endif
endfunction
function [f]=fact_iter(n)
    f=1;
    for i=1:n,
        f *= i;
    endfor
endfunction
disp(fact_rec(n))
disp(fact_iter(n))