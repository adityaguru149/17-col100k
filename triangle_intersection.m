i=0;
function flag = sameside(p1,p2,a,b)
    % sameside : returns true if the p1,p1 lie on same sides of the
    % edge ab and false otherwise
    p1(3) = 0; p2(3) = 0; a(3) = 0; b(3) = 0;
    cp1 = cross(b-a, p1-a);
    cp2 = cross(b-a, p2-a);
    if(dot(cp1, cp2) >= 0)
        flag = true;
    else
        flag = false;
    end
    disp(p1)
    disp(p2)
    disp(a)
    disp(b)
    flag
end


% triangle_test : returns true if the triangles overlap and false otherwise
% P1, P2: a 3 by 2 array (each), describing the vertices of a triangle,
% the first column corresponds to the x coordinates while the second column
% corresponds to the y coordinates 
function flag = triangle_intersection1(P1,P2)
    if length(intersect(P1,P2,"rows")) > 0
        flag = true; return;
    end
    P1(4:5,:) = P1(1:2,:);
    P2(4:5,:) = P2(1:2,:);

    flag = true;
    % Testing all the edges of P1
    for i=1:3
        if(~sameside(P1(i,:), P2(1,:), P1(i+1,:), P1(i+2,:)) ...
                && sameside(P2(1,:), P2(2,:), P1(i+1,:), P1(i+2,:)) ...
                && sameside(P2(2,:), P2(3,:), P1(i+1,:), P1(i+2,:)))
            flag = false; return;
        end
    end
    % Testing all the edges of P2
    for i=1:3
        if(~sameside(P2(i,:), P1(1,:), P2(i+1,:), P2(i+2,:)) ...
                && sameside(P1(1,:), P1(2,:), P2(i+1,:), P2(i+2,:)) ...
                && sameside(P1(2,:), P1(3,:), P2(i+1,:), P2(i+2,:)))
            flag = false; return;
        end
    end
end

%%testing sameside
%sameside([1 1], [3 1], [2 0], [2 2])
%sameside([1 1], [2 1], [2 0], [2 2])
%sameside([2 1], [3 1], [2 0], [2 2])
%
%%testcases
%printf("triangle_intersection\n")
%line overlap
x1=[1 1 3];
y1=[1 2 3];
x2=[1 3 3];
y2=[1 2 3];
triangle_intersection1([x1' y1'], [x2' y2'])

%%point overlap
%x1=[0 1 0];
%y1=[0 1 2];
%x2=[2 1 2];
%y2=[0 1 2];
%triangle_intersection1([x1' y1'], [x2' y2'])
P1 = [0  -0; 0   1; 1   0];
P2 = [-0  1; 1  -0; 1   1];
triangle_intersection1(P1, P2)
%seems this method works only area overlap and so we need to check beforehand if any points overlap