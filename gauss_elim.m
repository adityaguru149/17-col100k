xyz=0;
function[x]=myGauss(a)
    [m,n]=size(a);
    for j=1:m-1
        for z=2:m
            if a(j,j)==0
                t=a(j,:);a(j,:)=a(z,:);
                a(z,:)=t;
            end
        end
        for i=j+1:m
            a(i,:)=a(i,:)-a(j,:)*(a(i,j)/a(j,j));
        end
    end
    x=zeros(1,m);
    for s=m:-1:1
        c=0;
        for k=2:m
            c=c+a(s,k)*x(k);
        end
        x(s)=(a(s,n)-c)/a(s,s);
    end
end
eq = input('');
ans_vector = myGauss(eq)
printf("%s\n", mat2str(ans_vector, 5));

%input=[1 1 3; 3 -2 4]
%input=[1 -2 1 0; 2 1 -3 5; 4 -7 1 -1]
%input=[3 4 -2 2 2;4 9 -3 5 8;-2 -3 7 6 10;1 4 6 7 2]
