a = input(''); x = input('');
function [s] = myAns(a, x)
    if length(a) == 0
        s = false;
        return
    elseif length(a)==1
        s = a(1)==x;
        return
    endif
    s = (a(1)==x || myAns(a(2:end), x));
    return
endfunction
printf("%s", mat2str((myAns(a, x)))) %logical