# python3 expression.py
for i in range(1, 21):
	expr = input().strip()
	st = []
	valid = True
	bracket_starts = [ '(', '{', '<', '[' ]
	for ch in expr:
		if ch == ')':
			if len(st)<3:
				valid = False
				break
			op2 = st.pop()
			op1 = st.pop()
			oprnd = st.pop()
			if op2 not in bracket_starts and op1 not in bracket_starts and oprnd != '(':
				valid = False
				break
			val = op1 + op2
			st.append(val)
		elif ch == '}':
			if len(st)<3:
				valid = False
				break
			op2 = st.pop()
			op1 = st.pop()
			oprnd = st.pop()
			if op2 not in bracket_starts and op1 not in bracket_starts and oprnd != '{':
				valid = False
				break
			val = op1 - op2
			st.append(val)
		elif ch == '>':
			if len(st)<3:
				valid = False
				break
			op2 = st.pop()
			op1 = st.pop()
			oprnd = st.pop()
			if op2 not in bracket_starts and op1 not in bracket_starts and oprnd != '<':
				valid = False
				break
			val = op1 * op2
			st.append(val)
		elif ch == ']':
			if len(st)<3:
				valid = False
				break
			op2 = st.pop()
			op1 = st.pop()
			oprnd = st.pop()
			if op2 not in bracket_starts and op1 not in bracket_starts and oprnd != '[':
				valid = False
				break
			val = op1 // op2  #change to // for quotient
			st.append(val)
		elif ch in bracket_starts:
			st.append(ch)
		elif ch >= 'a' and ch <= 'z':
			st.append(ord(ch) - 96)
		else:
			valid = False
			break
		# print(st)
	out = 'INVALID'
	if valid and len(st) == 1 and isinstance(st[-1], int):
		out = st.pop()
	# print(out)
	print('case=Test {}\ninput={}\noutput="{}"\n'.format(i, expr, out))
