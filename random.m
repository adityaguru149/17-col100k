x = [1 2; 3.5 5; 0 7; 2 3; 5 6];
y = [0 1; -0.5 1; 0 0; 1 0; 1 0];
n = size(x, 1);
%hold on;
for i=1:n
    line(x(i,:), y(i,:));
end;
%hold off;
a = [x(:, 1) y(:, 1) x(:,2)-x(:,1) y(:,2)-y(:,1)];
printf("%s\n", mat2str(a))