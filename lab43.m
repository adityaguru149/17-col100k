xs = [-6 6 0];
ys = [0 0 6];
it=1;
global countb countt;
countb = 0;
countt = 0;
function meshb(x,y,L)
  global countb;
  if L == 0
    fill(x,y,'y');
    countb = countb+1;
  else
    a = [(x(1)+x(2))/2 (x(2)+x(3))/2 (x(3)+x(1))/2];
    b = [(y(1)+y(2))/2 (y(2)+y(3))/2 (y(3)+y(1))/2];
    fill(a,b,'m');
    meshb([x(1) a(1) a(3)],[y(1) b(1) b(3)],L-1);
    meshb([x(2) a(2) a(1)],[y(2) b(2) b(1)],L-1);
    meshb([x(3) a(3) a(2)],[y(3) b(3) b(2)],L-1);
  end
end

function mesht(x,y,L)
  global countt;
  if L == 0
    fill(x,y,'y');
    countt = countt+1;
  else
    cx = (x(1)+x(2)+x(3))/3;
    cy = (y(1)+y(2)+y(3))/3;
    line1 = linspace(x(1),x(2),4);
    line2 = linspace(x(2),x(3),4);
    line3 = linspace(x(3),x(1),4);
    a1 = [line1(2) line2(2) line3(2)];
    a2 = [line1(3) line2(3) line3(3)];
    line1 = linspace(y(1),y(2),4);
    line2 = linspace(y(2),y(3),4);
    line3 = linspace(y(3),y(1),4);
    b1 = [line1(2) line2(2) line3(2)];
    b2 = [line1(3) line2(3) line3(3)];
    fill([a2(3) a1(1) cx],[b2(3) b1(1) cy],'m');
    fill([a2(1) a1(2) cx],[b2(1) b1(2) cy],'m');
    fill([a2(2) a1(3) cx],[b2(2) b1(3) cy],'m');
    mesht([x(1) a1(1) a2(3)],[y(1) b1(1) b2(3)],L-1);
    %mesht([a2(3) a1(1) cx],[b2(3) b1(1) cy],L-1);
    mesht([a1(1) a2(1) cx],[b1(1) b2(1) cy],L-1);
    %mesht([a2(1) a1(2) cx],[b2(1) b1(2) cy],L-1);
    mesht([x(2) a1(2) a2(1)],[y(2) b1(2) b2(1)],L-1);
    mesht([a1(3) a2(3) cx],[b1(3) b2(3) cy],L-1);
    %mesht([a2(2) a1(3) cx],[b2(2) b1(3) cy],L-1);
    mesht([a1(2) a2(2) cx],[b1(2) b2(2) cy],L-1);
    mesht([x(3) a1(3) a2(2)],[y(3) b1(3) b2(2)],L-1);
  end
end

printf("%d %d", countb, countt)
%visuals
figure(1);
hold on;
meshb(xs,ys,it);
hold off;
figure(2);
hold on;
mesht(xs,ys,it);
hold off;
