xyz = 0;

function [X, Y] = hull(x, y)
k = convhull(x, y);
X = x(k); X = X(1:end-1);
Y = y(k); Y = Y(1:end-1);
end;

%returning second max/ min
function [y, z] = min_second_min(x)
   [y, i] = min(x);
   z = min(x(1:end ~= i));
end;
function [y, z] = max_second_max(x)
   [y, i] = max(x);
   z = max(x(1:end ~= i));
end;


for i=1:5,
    x = 10*randn(10, 1);
    y = 10*randn(10, 1);
    if i==5, x = [1; 2; 2; 1]; y=[1; 1; 2; 2]; end;  %handling special case  
    [x, y] = hull(x, y);
    if length(x)<4,
        continue;
    end;
    a = polyarea(x, y);
    fx = x(1);
    fy = y(1);
    a_tri=[];
    for j=3:length(x),
        ax = [fx; x(j-1:j)];
        ay = [fy; y(j-1:j)];
        arrea = polyarea(ax, ay);
        a_tri(end+1) = arrea;
    end;
    printf('case = Test %d\ninput =%s\n%s\n', i, mat2str(x'), mat2str(y'));
    [b, d] = max_second_max(a_tri);
    [c, e] = min_second_min(a_tri);
    printf('output =\"%.4f %.4f %.4f %.4f %.4f %.4f\"\n', a, sum(a_tri), b, c, d, e);
end;