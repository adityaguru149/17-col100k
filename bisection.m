xyz=0;

%function y = f1(x)
%	y = tan(x) - 2*x;
%return
%end
%
%function y = f2(x)
%	y = 65*x^4-72*x^3-1.5*x^2+16.5*x-1;
%return
%end
%
%function y = f3(x)
%	y = x^3-6*x^2+12*x-8;
%return
%end

f1 = @(x) (tan(x) - 2*x);
f2 = @(x) (65*x^4-72*x^3-1.5*x^2+16.5*x-1);
f3 = @(x) (x^3-6*x^2+12*x-8);

function [y,iter]=bisec(f,a,b,tol)
iter=0;
if feval(f,a)*feval(f,b)>0
% Check if we meet conditon for bisection method
y=-1; return
end
if(abs(b-a)<=tol)
% Are we starting with a solution ?
y=-1; return
end
while(abs(b-a)>tol)
    iter+=1;
    c=(a+b)/2;  % The midpoint
    % Which inteval has the root?
    if (feval(f,a)*feval(f,c)<=0)
    b=c;
    else
    a=c;
    end
end
y=c;  % return the best guess.
return
end

f = input('');
a = input('');
b = input('');
tol = input('');
[ans, i] = bisec(f, a, b, tol);
printf("%.4f\n%d\n",ans, i)