for cas=1:20
last=randi([4,700]); iterations = randi([2,(last+1)/2]);
printf("case = Test %d\ninput=%d\n%d\n",cas, iterations, last)

%last=11; iterations=4;
A=zeros(iterations,last);
mid=floor(last/2)+1;
A(1, mid)=1;
for k=2:iterations
    for j=1:last
        if j==1
            A(k,j)= xor(A(k-1,j+1),A(k-1,j));
        elseif j==last
            A(k,j)= xor(A(k-1,j),A(k-1,j-1));
        else
            A(k,j)= xor(A(k-1,j),xor(A(k-1,j+1),A(k-1,j-1)));
        end;
    end;
end;
%disp(A)
%I= mat2gray(A);
%imshow(I)

%calculate ratio
i=1; n1=0; total=0;
%total can be calculated using sum of odd integers formula
for k=1:iterations
    total+=i;
    i+=2;
    for j=first:last
        if A(k,j)==1
            n1++;
        end;
    end;
end;
%ratio=n1/(total);
%printf("%.2f", ratio)
%printf("%d %d", n1, total)

printf("output=/.*%d %d.*/\n", n1, total)
end;